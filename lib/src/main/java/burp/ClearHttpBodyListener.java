package burp;

public class ClearHttpBodyListener extends AbstractChangeListener {

    public ClearHttpBodyListener(
        IHttpRequestResponse requestResponse,
        IExtensionHelpers helpers)
    {
        super(requestResponse, helpers);
    }

    @Override
    void handleChange() {
        requestResponse.setRequest(helpers.buildHttpMessage(requestInfo.getHeaders(), null));
    }
    
}
