/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package burp;

public class BurpExtender implements IBurpExtender {

    private static final String extensionName = "Burp Editor Utils";

    @Override
    public void registerExtenderCallbacks(IBurpExtenderCallbacks callbacks) {
        callbacks.setExtensionName(extensionName);
        callbacks.registerContextMenuFactory(new ContextMenu(callbacks));
        callbacks.printOutput(extensionName);
    }
}
