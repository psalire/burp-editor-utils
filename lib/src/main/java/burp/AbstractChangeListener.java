package burp;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public abstract class AbstractChangeListener implements ActionListener {

    protected IHttpRequestResponse requestResponse;
    protected IRequestInfo requestInfo;
    protected IHttpService httpService;
    protected IExtensionHelpers helpers;

    public AbstractChangeListener(IHttpRequestResponse requestResponse, IExtensionHelpers helpers) {
        this.helpers = helpers;
        this.requestResponse = requestResponse;
        this.requestInfo = helpers.analyzeRequest(requestResponse);
        this.httpService = requestResponse.getHttpService();
    }

    abstract void handleChange();

    @Override
    public void actionPerformed(ActionEvent e) {
        handleChange();
    }
}
