package burp;

import java.util.List;

public class HttpEditorUtils {
    private HttpEditorUtils() {}

    /**
     * Change header value, or add if DNE, in-place
     * @param headersList
     * @param headerName
     * @param newHeaderValue
     */
    public static void addOrChangeHttpHeader(List<String> headersList, String headerName, String newHeaderValue) {
        String newHeader = headerName + ": " + newHeaderValue;
        boolean hasHeader = false;
        for (int i=0; i<headersList.size(); i++) {
            String header = headersList.get(i).toLowerCase();
            if (header.startsWith(headerName.toLowerCase())) {
                headersList.set(i, newHeader);
                hasHeader = true;
                break;
            }
        }
        if (!hasHeader) {
            headersList.add(newHeader);
        }
    }
    
    /**
     * Add http header iff DNE
     * @param headersList
     * @param headerName
     * @param newHeaderValue
     */
    public static void addHttpHeader(List<String> headersList, String headerName, String headerValue) {
        for (int i=0; i<headersList.size(); i++) {
            String header = headersList.get(i).toLowerCase();
            if (header.startsWith(headerName.toLowerCase())) {
                return;
            }
        }
        
        headersList.add(headerName + ": " + headerValue); // Header was not found, so add it
    }
    
    /**
     * Delete http header
     * @param headersList
     * @param headerName
     * @param newHeaderValue
     */
    public static void deleteHttpHeader(List<String> headersList, String headerName) {
        int i = 0;
        for (; i<headersList.size(); i++) {
            String header = headersList.get(i).toLowerCase();
            if (header.startsWith(headerName.toLowerCase())) {
                break;
            }
        }
        
        headersList.remove(i);
    }

    /**
     * Return HTTP version from a request string
     * @param request
     * @return Http Version
     */
    public static String getCurrentHttpVersion(String request) {
        return request.split("\r\n", 2)[0].split(" ")[2].trim();
    }
}
