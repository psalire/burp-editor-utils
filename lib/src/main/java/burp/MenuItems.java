package burp;

import java.util.Arrays;
import java.util.List;

public class MenuItems {
    private static List<Pair<String>> httpMethodsList = Arrays.asList(
        new Pair<>("GET"),
        new Pair<>("POST"),
        new Pair<>("PUT"),
        new Pair<>("DELETE"),
        new Pair<>("HEAD"),
        new Pair<>("CONNECT"),
        new Pair<>("OPTIONS"),
        new Pair<>("TRACE"),
        new Pair<>("PATCH")
    );
    private static List<Pair<String>> httpVersionsList = Arrays.asList(
        new Pair<>("HTTP/3"),
        new Pair<>("HTTP/2"),
        new Pair<>("HTTP/1.1"),
        new Pair<>("HTTP/1.0"),
        new Pair<>("HTTP/0.9")
    );
    private static List<Pair<String>> contentTypesList = Arrays.asList(
        new Pair<>("application/json"),
        new Pair<>("application/gzip"),
        new Pair<>("application/octet-stream"),
        new Pair<>("application/x-www-form-urlencoded"),
        new Pair<>("application/x-httpd-php"),
        new Pair<>("application/xml"),
        new Pair<>("application/zip"),
        new Pair<>("audio/mpeg"),
        new Pair<>("audio/webm"),
        new Pair<>("multipart/form-data"),
        new Pair<>("image/gif"),
        new Pair<>("image/jpeg"),
        new Pair<>("image/png"),
        new Pair<>("text/css"),
        new Pair<>("text/csv"),
        new Pair<>("text/javascript"),
        new Pair<>("text/html"),
        new Pair<>("text/plain"),
        new Pair<>("video/mp4"),
        new Pair<>("video/mpeg"),
        new Pair<>("video/webm")
    );
    private static List<Pair<String>> userAgentsList = Arrays.asList(
        new Pair<>(
            "Windows MS Edge",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.246"
        ),
        new Pair<>(
            "Linux Firefox",
            "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:15.0) Gecko/20100101 Firefox/15.0.1"
        ),
        new Pair<>(
            "macOS Chrome",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36"
        ),
        new Pair<>(
            "Windows Opera",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 OPR/89.0.4447.48"
        ),
        new Pair<>(
            "Google Bot",
            "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)"
        ),
        new Pair<>(
            "Facebook Bot",
            "facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)"
        ),
        new Pair<>(
            "Instagram",
            "Mozilla/5.0 (iPhone; CPU iPhone OS 15_5 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 Instagram 243.1.0.14.111 (iPhone12,1; iOS 15_5; en_US; en-US; scale=2.00; 828x1792; 382468104)"
        ),
        new Pair<>(
            "iOS Safari",
            "Mozilla/5.0 (iPhone12,1; U; CPU iPhone OS 13_0 like Mac OS X) AppleWebKit/602.1.50 (KHTML, like Gecko) Version/10.0 Mobile/15E148 Safari/602.1"
        ),
        new Pair<>(
            "Samsung Galaxy",
            "Mozilla/5.0 (Linux; Android 10; SM-G996U Build/QP1A.190711.020; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Mobile Safari/537.36"
        )
    );

    private MenuItems() {}

    public static List<Pair<String>> getHttpMethodsList() {
        return httpMethodsList;
    }
    public static List<Pair<String>> getHttpVersionsList() {
        return httpVersionsList;
    }
    public static List<Pair<String>> getContentTypesList() {
        return contentTypesList;
    }
    public static List<Pair<String>> getUserAgentsList() {
        return userAgentsList;
    }

    public static void addHttpMethod(String httpMethod) {
        httpMethodsList.add(new Pair<>(httpMethod));
    }
    public static void addHttpVersion(String httpVersion) {
        httpVersionsList.add(new Pair<>(httpVersion));
    }
    public static void addContentType(String contentType) {
        contentTypesList.add(new Pair<>(contentType));
    }
    public static void addUserAgent(String name, String value) {
        userAgentsList.add(new Pair<>(name, value));
    }
}
