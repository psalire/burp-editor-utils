package burp;

import java.util.ArrayList;
import java.util.Arrays;

public class ClearHttpHeaderListener extends AbstractChangeListener {

    private String headerName;

    public ClearHttpHeaderListener(
        IHttpRequestResponse requestResponse,
        IExtensionHelpers helpers,
        String headerName)
    {
        super(requestResponse, helpers);
        this.headerName = headerName.trim();
    }

    @Override
    void handleChange() {
        ArrayList<String> headersList = new ArrayList<>(requestInfo.getHeaders());
        
        HttpEditorUtils.deleteHttpHeader(headersList, headerName);

        byte[] request = requestResponse.getRequest();
        requestResponse.setRequest(
            helpers.buildHttpMessage(
                headersList,
                Arrays.copyOfRange(request, requestInfo.getBodyOffset(), request.length)
            )
        );
    }
    
}
