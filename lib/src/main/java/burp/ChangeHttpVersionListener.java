package burp;

public class ChangeHttpVersionListener extends AbstractChangeListener {

    private String currentVersion;
    private String newVersion;

    public ChangeHttpVersionListener(
        IHttpRequestResponse requestResponse,
        IExtensionHelpers helpers,
        String currentVersion,
        String newVersion) {
        super(requestResponse, helpers);
        
        this.currentVersion = currentVersion.trim();
        this.newVersion = newVersion.trim();
    }

    public String getHttpVersion() {
        return newVersion;
    }

    @Override
    void handleChange() {
        if (currentVersion.equals(newVersion)) {
            return;
        }

        String requestString = helpers.bytesToString(requestResponse.getRequest());
        String[] requestSplit = requestString.split("\r\n", 2);

        requestResponse.setRequest(
            helpers.stringToBytes(
                requestSplit[0].replace(currentVersion, newVersion) + "\r\n" + requestSplit[1]
            )
        );
    }
}
