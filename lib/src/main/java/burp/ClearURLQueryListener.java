package burp;

import java.util.ArrayList;
import java.util.Arrays;

public class ClearURLQueryListener extends AbstractChangeListener {

    public ClearURLQueryListener(
        IHttpRequestResponse requestResponse,
        IExtensionHelpers helpers)
    {
        super(requestResponse, helpers);
    }

    @Override
    void handleChange() {
        ArrayList<String> headersList = new ArrayList<>(requestInfo.getHeaders());
        headersList.set(
            0,
            String.format(
                "%s %s %s",
                requestInfo.getMethod(),
                requestInfo.getUrl().getPath(),
                HttpEditorUtils.getCurrentHttpVersion(new String(requestResponse.getRequest()))
            )
        );

        byte[] request = requestResponse.getRequest();
        requestResponse.setRequest(
            helpers.buildHttpMessage(
                headersList,
                Arrays.copyOfRange(request, requestInfo.getBodyOffset(), request.length)
            )
        );
    }
    
}
