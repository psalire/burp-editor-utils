package burp;

import java.util.ArrayList;
import java.util.Arrays;

public class ChangeUserAgentListener extends AbstractChangeListener {
    
    private String newUserAgent;

    public ChangeUserAgentListener(
        IHttpRequestResponse requestResponse,
        IExtensionHelpers helpers,
        String newUserAgent)
    {
        super(requestResponse, helpers);
        
        this.newUserAgent = newUserAgent.trim();
    }

    public String getContentType() {
        return newUserAgent;
    }

    @Override
    void handleChange() {
        ArrayList<String> headersList = new ArrayList<>(requestInfo.getHeaders());

        HttpEditorUtils.addOrChangeHttpHeader(
            headersList,
            "User-Agent",
            newUserAgent
        );

        byte[] request = requestResponse.getRequest();
        requestResponse.setRequest(
            helpers.buildHttpMessage(
                headersList,
                Arrays.copyOfRange(request, requestInfo.getBodyOffset(), request.length)
            )
        );
    }
}
