package burp;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class ContextMenu implements IContextMenuFactory {

    private IBurpExtenderCallbacks callbacks;
    private IExtensionHelpers helpers;
    
    public ContextMenu(IBurpExtenderCallbacks callbacks) {
        this.callbacks = callbacks;
        this.helpers = callbacks.getHelpers();
    }

    private void addMenuItems(
        IHttpRequestResponse request,
        JMenu menu,
        List<Pair<String>> values,
        String currentValue,
        BiFunction<String, String, AbstractChangeListener> fun)
    {
        for (Pair<String> pair : values) {
            String label = pair.getFirst();
            String value = pair.getSecond();
            JMenuItem menuItem = new JMenuItem(label);
            menuItem.addActionListener(fun.apply(currentValue, value));
            menu.add(menuItem);
        }
    }

    /* (non-Javadoc)
     * @see burp.IContextMenuFactory#createMenuItems(burp.IContextMenuInvocation)
     */
    @Override
    public List<JMenuItem> createMenuItems(IContextMenuInvocation invocation) {
        // Don't display menu in non-editable contexts
        byte invocationContext = invocation.getInvocationContext();
        if (invocationContext != IContextMenuInvocation.CONTEXT_INTRUDER_PAYLOAD_POSITIONS &&
            invocationContext != IContextMenuInvocation.CONTEXT_MESSAGE_EDITOR_REQUEST)
        {
            return null;
        }
        
        IHttpRequestResponse[] requestResponseList = invocation.getSelectedMessages();
        if (requestResponseList.length > 1) {
            callbacks.printError("Unexpected >1 IHttpRequestResponse, skipping the rest");
        }
        IHttpRequestResponse request = requestResponseList[0];

        // The top-level menu
        ArrayList<JMenuItem> menuItems = new ArrayList<JMenuItem>();
        
        // Create menus
        JMenu httpMethodsMenu = new JMenu("Change HTTP Method");
        JMenu httpVersionsMenu = new JMenu("Change HTTP Version");
        JMenu contentTypesMenu = new JMenu("Change Content-Type");
        JMenu userAgentsMenu = new JMenu("Change User-Agent");
        JMenu removeHeadersMenu = new JMenu("Remove HTTP Header");
        JMenuItem removeURLQueryMenuItem = new JMenuItem("Clear URL Query");
        JMenuItem removeHttpBodyMenuItem = new JMenuItem("Clear HTTP body");
        
        // Add action listeners i.e. onclick callbacks
        
        // TODO: change following to OOP
        BiFunction<String, String, AbstractChangeListener> createChangeHttpMethodListener = (a, b) -> new ChangeHttpMethodListener(request, helpers, b);
        BiFunction<String, String, AbstractChangeListener> createChangeHttpVersionListener = (a, b) -> new ChangeHttpVersionListener(request, helpers, a, b);
        BiFunction<String, String, AbstractChangeListener> createChangeContentTypeListener = (a, b) -> new ChangeContentTypeListener(request, helpers, b);
        BiFunction<String, String, AbstractChangeListener> createUserAgentListener = (a, b) -> new ChangeUserAgentListener(request, helpers, b);
        BiFunction<String, String, AbstractChangeListener> createClearHttpHeaderListener = (a, b) -> new ClearHttpHeaderListener(request, helpers, b);

        // Add items to menus
        addMenuItems(request, httpMethodsMenu, MenuItems.getHttpMethodsList(), null, createChangeHttpMethodListener);
        String currentHttpVersion = HttpEditorUtils.getCurrentHttpVersion(helpers.bytesToString(request.getRequest()));
        if (currentHttpVersion.equals("HTTP/2")) {
            // Burp doesn't let changing HTTP/2 version
            httpVersionsMenu.setText("Change HTTP Version (may not work if request is HTTP/2)");
        }
        addMenuItems(request, httpVersionsMenu, MenuItems.getHttpVersionsList(), currentHttpVersion, createChangeHttpVersionListener); 
        addMenuItems(request, contentTypesMenu, MenuItems.getContentTypesList(), null, createChangeContentTypeListener);
        addMenuItems(request, userAgentsMenu, MenuItems.getUserAgentsList(), null, createUserAgentListener);
        addMenuItems(request, removeHeadersMenu,
            new ArrayList<>(
                helpers.analyzeRequest(request).getHeaders()).stream()
                    .skip(1) // skip the HTTP line
                    .map(a -> new Pair<String>(a.split(":", 2)[0])) // Extract the header name
                    .collect(Collectors.toList()),
            null,
            createClearHttpHeaderListener
        );
        removeURLQueryMenuItem.addActionListener(new ClearURLQueryListener(request, helpers));
        removeHttpBodyMenuItem.addActionListener(new ClearHttpBodyListener(request, helpers));

        // Add everything to the top-level menu
        menuItems.add(httpMethodsMenu);
        // menuItems.add(httpVersionsMenu);
        menuItems.add(contentTypesMenu);
        menuItems.add(userAgentsMenu);
        // menuItems.add(removeHeadersMenu);
        menuItems.add(removeURLQueryMenuItem);
        menuItems.add(removeHttpBodyMenuItem);
        
        return menuItems;
    }
}
