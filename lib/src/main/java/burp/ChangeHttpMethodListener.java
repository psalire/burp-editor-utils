package burp;

import java.util.ArrayList;
import java.util.Arrays;

public class ChangeHttpMethodListener extends AbstractChangeListener {

    private String newMethod;

    public ChangeHttpMethodListener(
        IHttpRequestResponse requestResponse,
        IExtensionHelpers helpers,
        String newMethod) {
        super(requestResponse, helpers);
        
        this.newMethod = newMethod.trim();
    }

    public String getHttpMethod() {
        return newMethod;
    }

    @Override
    void handleChange() {
        if (requestInfo.getMethod().equals(newMethod)) {
            return;
        }
        
        ArrayList<String> headersList = new ArrayList<>(requestInfo.getHeaders());
        if (newMethod.equals("POST") ||
            newMethod.equals("PUT") ||
            newMethod.equals("PATCH") ||
            newMethod.equals("DELETE"))
        {
            HttpEditorUtils.addHttpHeader(
                headersList,
                "Content-Length",
                "0"
            );
        }

        byte[] httpRequest = requestResponse.getRequest();
        byte[] newHttpRequest = helpers.buildHttpMessage(
            headersList,
            Arrays.copyOfRange(httpRequest, requestInfo.getBodyOffset(), httpRequest.length)
        );
        
        String requestString = helpers.bytesToString(newHttpRequest);

        requestResponse.setRequest(
            helpers.stringToBytes(
                newMethod + requestString.substring(requestString.indexOf(' '))
            )
        );
    }
}
