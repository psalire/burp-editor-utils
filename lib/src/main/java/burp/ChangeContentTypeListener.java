package burp;

import java.util.ArrayList;
import java.util.Arrays;

public class ChangeContentTypeListener extends AbstractChangeListener {
    
    private String newContentType;

    public ChangeContentTypeListener(
        IHttpRequestResponse requestResponse,
        IExtensionHelpers helpers,
        String newContentType) {
        super(requestResponse, helpers);
        
        this.newContentType = newContentType.trim();
    }

    public String getContentType() {
        return newContentType;
    }

    @Override
    void handleChange() {
        ArrayList<String> headersList = new ArrayList<>(requestInfo.getHeaders());

        HttpEditorUtils.addOrChangeHttpHeader(
            headersList,
            "Content-Type",
            newContentType
        );

        byte[] request = requestResponse.getRequest();
        requestResponse.setRequest(
            helpers.buildHttpMessage(
                headersList,
                Arrays.copyOfRange(request, requestInfo.getBodyOffset(), request.length)
            )
        );
    }
}
