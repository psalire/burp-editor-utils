# Burp Editor Utils

A Burp extension that adds convenience functions to Burp's message editor.

![Screenshot](screenshot.jpg)

## Installation

Import the [lib-all.jar](lib/build/libs/lib-all.jar) file to Burp at Extender->Extensions->Add.

## Building Locally

Build with `gradle shadowJar` in directory `lib/`. If successful, `lib-all.jar` will be created at `lib/build/libs/`.

Works with `Gradle 6.9`; may or may not work with other versions.

## Development

### Adding new functions:

1. Create new class that extends `AbstractChangeListener.java`
2. If necessary, add a new `List` to `MenuItems.java` and related getters
3. Add a new `JMenu`, `JMenuItem`, etc. instance to `createMenuItems` in `ContextMenu.java`
4. Attach the class created in step 1 as the listener to the `JMenu` in step 3

### Adding new values:

You may want to add more values to the "Change Content-Type" or "Change User-Agent" list. This involves simply adding new Strings to `MenuItems.java`.

1. Identify the relevant `List` in `MenuItems.java`
2. Add new values to the list
